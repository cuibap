require 'erb'
require 'cuibap/config'
require 'cuibap/db'
require 'cuibap/partial'

class MonthEntry < Partial
  @@factories['MonthEntry'] = Proc.new do
    for id in EntryDB.db.all
      @@partials["MonthEntry:#{id}"] = MonthEntry.new(id)
    end
  end

  def initialize(id)
    super(C.archives_url+sprintf('%04d/%02d/',id.to_s[0,4].to_i,id.to_s[5,2].to_i))
    @entry = EntryDB.db[id]
  end

  def to_task
    namespace :entry do
      task @entry.id => EntryTask.define_task(@entry.id)
    end
  end

  def url
    @base_url + C.indexfile + '#' + @entry.id.to_s
  end

  def to_html
    title = CuiBapCloth.new(self, @entry.title, @entry.id).to_html
    htmlbody = @entry.to_html(self)
    permalink = partial_url('MonthEntry',@entry.id)
    source_link = resolve_url(@entry.path)
    is_draft = false
    if C.git_dir
      revs = %x(git --git-dir=#{C.git_dir} rev-list HEAD -- #{@entry.filename}).chomp.split(/\n/)
      if revs.length > 1
        author_line = %x(git --git-dir=#{C.git_dir} cat-file commit #{revs[0]}).split(/\n\n/)[0].split(/\n/).grep(/^author /)[0]
	author_line =~ /> +(\d+) ([0-9+-]+)/
	last_update = %x(date -d 'TZ="#{$2}" @#{$1}')
	log_dir = File.join(C.archives_dir,'changes')
	log_path = File.join(log_dir,"#{@entry.id}.html")
	File.mkdir(log_dir) unless File.directory?(log_dir)
	f = File.open(log_path,'w')
	changes = %x(git --git-dir=#{C.git_dir} rev-list HEAD -- #{@entry.filename}|sed '$d'|git --git-dir=#{C.git_dir} diff-tree --stdin -r -p --pretty -- #{@entry.filename})
	changes = ERB::Util::html_escape(changes)
	changes.gsub!(/^(?:commit|Author:|Date:|diff|index|\+\+\+|---) .*/) { |x| "<b>#{x}</b>" }
	changes.gsub!(/^    .*/) {|x| "<i>#{x}</i>"}
	changes.gsub!(/^@@.*/) { |x| "<span class=\"diff-context\">#{x}</span>" }
	changes.gsub!(/^-.*/) { |x| "<span class=\"diff-removed\">#{x}</span>" }
	changes.gsub!(/^\+.*/) { |x| "<span class=\"diff-added\">#{x}</span>" }
	f.puts <<EOF
<html>
<head>
<title>#{@entry.title}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style type="text/css">
.diff-added { color: #008800; }
.diff-removed { color: #cc0000; }
.diff-context { color: #990099; background-color: #ffeeff; }
</style>
</head>
<body>
<pre>#{changes}</pre>
</body>
</html>
EOF
	f.close
	log_link = resolve_url(log_path)
      end
      pwd = Dir.pwd
      git_dir = File.expand_path(C.git_dir)
      Dir.chdir(C.data_dir)
      is_draft = !%x(git --git-dir=#{git_dir} ls-files --others --modified #{@entry.filename}).chomp.empty?
      Dir.chdir(pwd)
    end
    entry = @entry
    erb = ERB.new(File.read('templates/entry.rhtml'),nil,'-')
    erb.result(binding)
  end
end
