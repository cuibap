require 'rubygems'
require 'rake'
require 'cuibap/config'

class Partial
  @@factories = {}
  @@partials = {}

  def initialize(base_url)
    @base_url = base_url
  end

  def to_task
  end

  def url
    @base_url+C.indexfile
  end

  def to_html
  end

  def resolve_url(url)
    if C.absolute_links
      C.url+url
    else
      '../'*@base_url.gsub(/\/\/*/,'/').count('/')+url
    end
  end

  def partial_url(partial, id)
    part = Partial.get_partial(partial,id)
    url = part.url unless part.nil?
    url.nil? ? '' : resolve_url(url)
  end

  def Partial.get_partial(factory,id)
    return @@partials["#{factory}:#{id}"]
  end

  def Partial.load
    @@factories.keys.each do |x|
      puts "Init #{x}"
      @@factories[x].call
    end
  end

  def Partial.config
    C
  end
end
