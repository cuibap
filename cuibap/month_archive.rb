require 'erb'
require 'cuibap/config'
require 'cuibap/db'
require 'cuibap/partial'

class MonthArchive < Partial
  @@factories['MonthArchive'] = Proc.new do
    db = EntryDB.db
    unless db.empty?
      for year in db.get_min_year..db.get_max_year
        for month in 1..12
          next if db.entries_by_prefix(sprintf('%04d-%02d-',year,month)).empty?
          @@partials['MonthArchive:'+sprintf('%04d%02d',year,month)] = MonthArchive.new(year,month)
        end
      end
    end
  end

  def initialize(year,month)
    super(C.archives_url+sprintf('%04d/%02d',year,month)+'/')
    @year = year
    @month = month
    target_dir = C.archives_dir + sprintf('%04d/%02d',@year,@month)
    desc "Directory #{@month}/#{@year}"
    directory target_dir
    target = target_dir + '/' + C.indexfile
    file target => target_dir
    task :archive => target
    entries = EntryDB.db.entries_by_prefix(sprintf('%04d-%02d-',@year,@month))
    desc "Month archive #{month}/#{year}"
    file target => entries_to_tasks(entries) do |task|
      html = self.to_html
      f = File.new(task.name, 'w')
      f.write(html)
      f.close
    end
  end

  def prerequisites
  end

  def to_html
    db = EntryDB.db
    prefix=sprintf('%04d-%02d-',@year,@month)
    entries = db.entries_by_prefix(prefix)
    blog_content = ''
    recent_entries = ''
    entries.each do |e|
      blog_content += Partial.get_partial('MonthEntry',e).to_html
      recent_entries += %|<a href="##{e}">#{db[e].title}</a><br/>|
    end

    tag_links = ''
    tags = db.build_custom_tags(entries)
    tags.keys.each do |tag|
      tag_entries = tags[tag]
      if tag_entries.length > 0
        url = partial_url('TagArchive',tag)
        tag_links += %|<a href="#{url}">#{tag}</a> (#{tag_entries.length}) |
      end
    end

    calendar = Calendar.new(@year,@month).to_html

    next_month = @month + 1 if @month < 12
    prev_month = @month - 1 if @month > 1

    template = File.read(C.rootdir+'templates/month_archive.rhtml')
    @erb = ERB.new(template, nil, '-')
    content = @erb.result(binding)
    template = File.read(C.rootdir+'templates/layout.rhtml')
    @erb = ERB.new(template, nil, '-')
    @erb.result(binding)
  end
end
