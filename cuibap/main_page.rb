require 'erb'
require 'cuibap/config'
require 'cuibap/db'
require 'cuibap/partial'
require 'cuibap/month_entry'
require 'cuibap/calendar'

class MainPageEntry < MonthEntry
  def initialize(id)
    super(id)
    @base_url = ''
  end
end

class MainPageCalendar < Calendar
  def initialize(year,month)
    super(year,month)
    @base_url = ''
  end
end

class MainPage < Partial
  @@factories['MainPage'] = Proc.new do
    @@partials["MainPage:0"] = MainPage.new()
  end

  def initialize()
    super('')
    file C.main_link => entries_to_tasks(EntryDB.db.top_entries(C.max_entries))
    desc "Main page"
    file C.main_link => :metadata do |task| 
      html = self.to_html
      f = File.new(task.name, 'w')
      f.write(html)
      f.close
    end
  end

  def url
    C.main_link
  end

  def to_html
    db = EntryDB.db
      
    entries = db.top_entries(C.max_entries)
    blog_content = ''
    recent_entries = ''
    entries.each do |e|
      ee = MainPageEntry.new(e)
      blog_content += ee.to_html
      recent_entries += %|<a href="##{e}">#{db[e].title}</a><br/>|
    end
    current_prefix = entries[0].to_s[0,8]

    min_year = db.get_oldest_entry.to_s[0,4].to_i
    max_year = db.get_lastest_entry.to_s[0,4].to_i
    archive_links = ''
#  month = 12
    year = max_year
#  while month > 0
#    month_entries = db.entries_by_prefix(sprintf('%04d-%02d-',year,month))
#    if month_entries.length > 0
#      archive_path = ARCHIVES_DIR+sprintf('/%04d/%02d',year,month)
#      archive_links += %|<a href="#{base}#{archives_url}/#{index}">#{month}/#{year}</a> (#{month_entries.length})<br/>|
#    end
#    month -= 1
#  end
    while year >= min_year
      year_entries = db.entries_by_prefix(year.to_s+'-')
      if year_entries.length > 0
        url = partial_url('YearIndex',year)
        archive_links += %|<a href="#{url}">#{year}</a> (#{year_entries.length})<br/>|
      end
      year -= 1
    end

    tag_links = ''
    db.tags.keys.each do |tag|
      tag_entries = db.tags[tag]
      if tag_entries.length > 0
        url = partial_url('TagArchive',tag)
        tag_links += %|<a href="#{url}">#{tag}</a> (#{tag_entries.length}) |
      end
    end

    calendar = MainPageCalendar.new(max_year,db.get_max_month(max_year)).to_html

    blog_status = <<EOF
Tổng số bài: #{db.entries_num}<br/>
Bài cuối: #{db[db.get_lastest_entry].date}<br/>
Lần cập nhật cuối: Không biết<br/>
EOF

    template = File.read(C.rootdir+'templates/main_index.rhtml')
    @erb = ERB.new(template, nil, '-')
    content = @erb.result(binding)
    template = File.read(C.rootdir+'templates/layout.rhtml')
    @erb = ERB.new(template, nil, '-')
    @erb.result(binding)
  end
end

