require 'erb'
require 'cuibap/config'
require 'cuibap/db'
require 'cuibap/partial'

class TagArchive < Partial
  @@factories['TagArchive'] = Proc.new do
    EntryDB.db.build_tags
    for tag in EntryDB.db.tags.keys
      @@partials["TagArchive:#{tag}"] = TagArchive.new(tag)
    end
  end

  def initialize(id)
    @tag = id
    @tag_code = Entry.get_tag_code(@tag)
    super(C.tags_url+@tag_code+'/')
    target_dir = C.tags_url+@tag_code
    directory target_dir

    target = target_dir + '/' + C.indexfile
    desc "Build tag archive for #{@tag}"
    file target

    task :archive => target

    file target => target_dir
    file target => entries_to_tasks(EntryDB.db.tags[@tag]) unless EntryDB.db.tags[@tag].nil?
    file target => :metadata do |task|
      html = self.to_html
      f = File.new(task.name, 'w')
      f.write(html)
      f.close
    end
  end

  def to_html
    db = EntryDB.db
    article_links = ''
    entries = db.entries_by_tag(@tag)
    for e in entries
      year = e.to_s[0,4].to_i
      month = e.to_s[5,2].to_i
      month_url = partial_url('MonthArchive',sprintf('%04d%02d',year,month))
      url = partial_url('MonthEntry',e)
      article_links += %|<a href="#{month_url}">#{month} / #{year}</a>|
      article_links += ' - '
      article_links += %|<a href="#{url}">#{db[e].title}</a>|
      article_links += ' - '
      article_links += db[e].tags.map { |x| %|<a href="#{partial_url('TagArchive',x)}">#{x}</a>|}.join(', ') unless db[e].tags.empty?
      article_links += '<br/>'
    end

    template = File.read(C.rootdir+'templates/tag_archive.rhtml')
    @erb = ERB.new(template, nil, '-')
    content = @erb.result(binding)
    template = File.read(C.rootdir+'templates/layout.rhtml')
    @erb = ERB.new(template, nil, '-')
    @erb.result(binding)
  end
end
