require 'erb'
require 'cuibap/config'
require 'cuibap/db'
require 'cuibap/partial'
require 'cuibap/month_entry'
require 'cuibap/main_page'

class TestPage < Partial
  def initialize(id)
    super('')
    @id = id
  end

  def to_html
    db = EntryDB.db
    blog_content = MainPageEntry.new(@id).to_html
    template = File.read(C.rootdir+'templates/main_index.rhtml')
    @erb = ERB.new(template, nil, '-')
    content = @erb.result(binding)
    template = File.read(C.rootdir+'templates/layout.rhtml')
    @erb = ERB.new(template, nil, '-')
    @erb.result(binding)
  end
end
