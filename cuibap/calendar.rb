require 'erb'
require 'cuibap/config'
require 'cuibap/db'
require 'cuibap/partial'

class Calendar < Partial
  @@factories['Calendar'] = Proc.new do
    db = EntryDB.db
    unless db.empty?
      for year in db.get_min_year..db.get_max_year
        for month in 1..12
          next if db.entries_by_prefix(sprintf('%04d-%02d-',year,month)).empty?
          @@partials['Calendar:'+sprintf('%04d%02d',year,month)] = Calendar.new(year,month)
        end
      end
    end
  end

  def initialize(year, month)
    super(C.archives_url+sprintf('%04d/%02d',year,month)+'/')
    @year = year
    @month = month
  end

  def to_html
    html = ''
    cal = %x(cal #{@month} #{@year})
    cal.chomp!
    lines = cal.split(/\n/)
    html += '<table border="0" cellspacing="4" cellpadding="0" summary="Calendar with links to days with entries">'
    html += %|<caption class="calendarhead">#{lines[0].strip}</caption>|
    html += '<tr>'
    lines[1].split(/  */).each do |wd|
      html += %|<th style="text-align: center;"><span class="calendarday">#{wd}</span></th>|
    end
    html += '</tr>'
    
    lines[2..lines.length].each do |l|
      html += '<tr>'
      l.gsub!(/  [ \t]/,'<td style="text-align: center;"></td>')
      l.gsub!(/([0-9]+)/,'<td style="text-align: center;"><span class="calendar">\1</span></td>')
      html += l
      html += '</tr>'
    end
    html += '</table>'
    prefix = sprintf('%04d-%02d-',@year,@month)
    entries = EntryDB.db.entries_by_prefix(prefix)
    for i in 1..31
      a = entries.find { |x| x.to_s[8,2].to_i == i }
      unless a.nil?
        url = partial_url('MonthEntry',a)
        html.sub!(Regexp.new(">#{i}<"),%|><a href="#{url}">#{i}</a><|)
      end
    end
    html
  end
end
