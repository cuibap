require 'erb'
require 'cuibap/config'
require 'cuibap/db'
require 'cuibap/partial'

class YearIndex < Partial
  @@factories['YearIndex'] = Proc.new do
    db = EntryDB.db
    unless db.empty?
      for year in db.get_min_year..db.get_max_year
        next if db.entries_by_prefix(sprintf('%04d',year)).empty?
        @@partials['YearIndex:'+sprintf('%04d',year)] = YearIndex.new(year)
      end
    end
  end

  def initialize(year)
    super(C.archives_url+sprintf('%04d',year)+'/')
    @year = year
    target_dir = C.archives_dir + sprintf('%04d',@year)
    desc "Directory #{@year}"
    directory target_dir
    target = target_dir + '/' + C.indexfile
    file target => target_dir
    task :archive => target
    entries = EntryDB.db.entries_by_prefix(sprintf('%04d-',@year))
    desc "Year archive #{year}"
    file target => entries_to_tasks(entries) do |task|
      html = self.to_html
      f = File.new(task.name, 'w')
      f.write(html)
      f.close
    end
  end

  def prerequisites
  end

  def to_html
    entries = []
    for month in 1..12
      prefix=sprintf('%04d-%02d-',@year,month)
      entries[month] = EntryDB.db.entries_by_prefix(prefix)
    end

    month_links = ''
    month = 12
    while month > 0
      url = partial_url('MonthArchive',sprintf('%04d%02d',@year,month))
      month_links += %|<a href="#{url}">Tháng #{month}</a> (#{entries[month].length})<br/>|
      month -= 1
    end

    article_links = ''
    month = 12
    while month > 0
      entries[month].each do |e|
        month_url = partial_url('MonthArchive',sprintf('%04d%02d',@year,month))
        url = partial_url('MonthEntry',e)
        article_links += %|<a href="#{month_url}">#{month} / #{@year}</a>|
	article_links += ' - '
	article_links += %|<a href="#{url}">#{EntryDB.db[e].title}</a>|
	article_links += ' - '
	article_links += EntryDB.db[e].tags.map { |x| %|<a href="#{partial_url('TagArchive',x)}">#{x}</a>|}.join(', ') unless EntryDB.db[e].tags.empty?
	article_links += '<br/>'
      end
      month -= 1
    end

    min_year = EntryDB.db.get_max_year
    max_year = EntryDB.db.get_min_year
    next_year = @year + 1 if @year < max_year
    prev_year = @year - 1 if @year > min_year

    template = File.read(C.rootdir+'templates/year_archive.rhtml')
    @erb = ERB.new(template, nil, '-')
    content = @erb.result(binding)
    template = File.read(C.rootdir+'templates/layout.rhtml')
    @erb = ERB.new(template, nil, '-')
    @erb.result(binding)
  end
end
