task :edit => :metadata do
  db = EntryDB.db
  ids = $CB_ARGV.
    find_all{|x| x[0] !~ /^-/}.
    map {|x| db.id_from_hid(x.to_i)}
  ids << db.id_from_hid(1) if ids.empty?
  if ids.empty?
    puts "Wrong number"
  else
    pathnames = ids.map { |x| "#{C.data_dir}/#{x.to_s}.txt" }
    editor = C.editor
    editor = ENV['EDITOR'] if ENV.member? 'EDITOR'
    editor = 'vim' if editor.nil? or editor.empty?
    system("#{editor} #{pathnames.join(' ')}")
  end
end
