task :commit do
  if C.git_dir.nil? or C.git_dir.empty?
    puts "This command is only available if you have a Git repository"
  else
    db = EntryDB.db
    argv = $CB_ARGV.dup
    if $CB_ARGV.index('-a') != nil
      entries = git_status
      for status,entry in entries
        argv << entry.hid.to_s
      end
    end
    ids = argv.
      find_all{|x| x[0] !~ /^-/}.
      map do |x|
        hid = x.to_i
        puts "Wrong ID #{x}" if hid.nil?
        id = db.id_from_hid(hid)
        puts "Wrong ID #{x}" if id.nil?
        e = db[id]
        if e.nil?
          puts "Entry not found, probably wrong ID #{id}"
          nil
        else
          e.filename
        end
      end
    if ids.nil? or ids.index(nil) != nil
      puts "Wrong ID"
    else
      ENV['GIT_DIR'] = File.expand_path(C.git_dir)
      Dir.chdir(C.data_dir)
      # check for untracked files
      cmdline = "git ls-files -o -- #{ids.join(' ')}"
      #puts cmdline
      untracked_files = %x(#{cmdline}).chomp.split(/\n/)
      unless untracked_files.empty?
        cmdline = "git update-index --add -- #{untracked_files.join(' ')}"
        #puts cmdline
        system(cmdline)
      end
      cmdline = "git commit #{ids.join(' ')}"
      #puts cmdline
      system(cmdline)
    end
  end
end
