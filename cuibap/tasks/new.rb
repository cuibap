task :new do 
  id = %x(date "+%Y-%m-%dT%H_%M_%S").chomp
  date = %x(date).chomp
  author = C.author
  pathname = "#{C.data_dir}/#{id}.txt"
  unless File.exists? pathname
    f = File.new(pathname,'w')
    f.write <<EOF
TITLE: Bài mới
AUTHOR: #{author}
DESC:
DATE: #{date}
TAGS:
FORMAT: textile
-----
BODY:
-----
EOF
    f.close
  end
  editor = C.editor
  editor = ENV['EDITOR'] if ENV.member? 'EDITOR'
  editor = 'vim' if editor.nil? or editor.empty?
  system("#{editor} #{C.data_dir}/#{id}.txt")
end
