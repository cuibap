task :status do
  if C.git_dir.nil? or C.git_dir.empty?
    puts "This command is only available if you have a Git repository"
  else
    entries = git_status
    for status,e in entries
      puts "[#{e.hid}]\t#{status}\t#{e.title} - #{e.tags.join(", ")}"
    end
  end
end
