task :rm => :metadata do |task|
  if $CB_ARGV.empty?
    puts "please specify one id to delete"
  else
    db = EntryDB.db
    id = db.id_from_hid($CB_ARGV[0].to_i)
    if id.nil? or (not id.instance_of? Symbol)
      puts "Wrong number"
    else
      pathname = "#{C.data_dir}/#{id.to_s}.txt"
      puts "removing #{db[id].title}"
      File.delete pathname
    end
  end
end
