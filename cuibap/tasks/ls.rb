task :ls => :metadata do
  num = ENV['N'].to_i
  num = 10 if num < 2
  db = EntryDB.db
  entries = db.top_entries(num)
  for id in entries
    e = db[id]
    puts "[#{e.hid}] #{e.title} - #{e.tags.join(", ")}"
  end
end
