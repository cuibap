task :grep do
  db = EntryDB.db
  cmdline = "grep -l #{$CB_ARGV.map{|x| x.inspect}.join(' ')} #{C.data_dir}/*.txt"
  list = %x(#{cmdline}).
    chomp.
    split(/\n/).
    map {|x| db.id_from_path(x)}.
    each do |id|
      e = db[id]
      puts "[#{e.hid}] #{e.title} - #{e.tags.join(", ")}"
    end
end
