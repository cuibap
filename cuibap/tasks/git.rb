task 'git' do
  ENV['GIT_DIR'] = File.expand_path(C.git_dir)
  Dir.chdir(C.data_dir)
  system "git #{ARGV[1..-1].join(' ')}"
end
