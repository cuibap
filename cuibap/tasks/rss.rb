file 'rss.xml' => C.main_link do |task| 
  channel = Object.new
  class << channel
    attr_accessor :title, :description, :link, :ttl, :items
  end
  channel.title = C.title
  channel.description = C.description
  channel.link = C.url
  channel.ttl = 180
  channel.items = []

  db = EntryDB.db
  for e in db.top_entries(C.max_entries)
    item = {}
    class << item
      attr_accessor :title, :date, :link, :author, :categories
    end
    item.link = "#{C.url}#{C.main_index}##{e.to_s}"
    item.title = db[e].title
    item.date = Time.parse(e.to_s.gsub('T',' ').gsub('_',':'))
    item.author = db[e].author
    item.categories = db[e].tags
    channel.items.push item
  end
  erb = ERB.new(File.read('templates/rss.rxml'),nil,'-')
  body = erb.result(binding)

  f = File.new(task.name, 'w')
  f.write(body)
  f.close
end

