desc 'Build test page'
task :test do |task|
  db = EntryDB.db
  id = db.id_from_hid($CB_ARGV.empty? ? 1 : $CB_ARGV[0].to_i)
  html = TestPage.new(id).to_html
  f = File.new('test.html', 'w')
  f.write(html)
  f.close
end
