def git_status
  results = []
  db = EntryDB.db
  git_dir= File.expand_path(C.git_dir)
  ENV['GIT_DIR'] = git_dir
  pwd = Dir.pwd
  Dir.chdir(C.data_dir)
  cmdline = "git ls-files -o"
  list = %x(#{cmdline}).
    chomp.
    split(/\n/).
    map {|x| db.id_from_path(x)}.
    each do |id|
      e = db[id]
      results << ['?',e] unless e.nil?
    end
  cmdline = "git diff-index --name-status HEAD"
  list = %x(#{cmdline}).
    chomp.
    split(/\n/).
    map {|x| x.split(/\t/)}.
    map {|x| [ x[0], db.id_from_path(x[1]) ]}.each do |x|
      e = db[x[1]]
      results << [x[0],e] unless e.nil?
    end
  Dir.chdir(pwd)
  results
end
