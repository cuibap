require 'cuibap/tasks'
def msg(text)
  puts text
  exit
end
msg 'cb help to learn how to use cuibap' if ARGV.empty?

task_name = ARGV[0]
$CB_ARGV = ARGV[1..-1]
msg "Unknown command #{task_name}" unless Rake::Task.task_defined? task_name
Rake::Task[task_name].invoke
