require 'rubygems'
require 'redcloth'

class CuiBapCloth < RedCloth
  RULES = [:glyphs_smileys, :textile, :markdown]

  def initialize(partial, string, id = nil, restrictions = [])
    super(string,restrictions)
    @partial = partial
    @id = id
  end

  def to_html
    @graphviz_list = []
    graphviz_it unless @id.nil?
    super(*RULES)
  end

  def CuiBapCloth.smileys_to_html(partial, text)
    smilies_url = partial.resolve_url('smilies/')
    text.gsub( SMILEY_REGEXP ) do |match|
      %|<img src="#{smilies_url}#{SMILEY_MAP[match]}.gif" alt="#{match}"/>|
    end
  end

  private

  SMILEY_MAP = {
      ':-@' => 'angry',
      '8-)' => 'cool',
      '8)' => 'cool',
      ':cheesy:' => 'cheesy',
      ':\'-(' => 'cry',
      ':*)' => 'drunk',
      ':embarassed:' => 'embarassed',
      ':grin:' => 'grin',
      ':huh:' => 'huh',
      ':-D' => 'laugh',
      ':D' => 'laugh',
      ':lol:' => 'laugh',
      ':kiss:' => 'kiss',
      ':-O' => 'oops',
      ':O' => 'oops',
      ':-(' => 'sad',
      ':(' => 'sad',
      '|-I' => 'sleep',
      ':-)' => 'smiley',
      ':)' => 'smiley',
      ':-P' => 'tongue',
      ':P' => 'tongue',
      ';-)' => 'wink',
      ';)' => 'wink'
    }
  SMILEY_REGEXP = Regexp.union( *SMILEY_MAP.keys.collect {|t| /#{Regexp.escape(t)}/ } )

  def glyphs_smileys(text)
    smilies_url = @partial.resolve_url('smilies/')
    text.gsub!( SMILEY_REGEXP ) do |match|
      %|<img src="#{smilies_url}#{SMILEY_MAP[match]}.gif" alt="#{match}"/>|
    end
  end

    # overwrite redcloth inline_textile_image due to incorrect check_refs call
    def inline_textile_image( text ) 
        text.gsub!( IMAGE_RE )  do |m|
            stln,algn,atts,url,title,href,href_a1,href_a2 = $~[1..8]
            url, url_title = check_refs( url )
            atts = pba( atts )
            atts = " src=\"#{ url }\"#{ atts }"
            atts << " title=\"#{ title }\"" if title
            atts << " alt=\"#{ title }\"" 
            # size = @getimagesize($url);
            # if($size) $atts.= " $size[3]";

            href, alt_title = check_refs( href ) if href

            out = ''
            out << "<a#{ shelve( " href=\"#{ href }\"" ) }>" if href
            out << "<img#{ shelve( atts ) } />"
            out << "</a>#{ href_a1 }#{ href_a2 }" if href
            
            if algn 
                algn = h_align( algn )
                if stln == "<p>"
                    out = "<p style=\"float:#{ algn }\">#{ out }"
                else
                    out = "#{ stln }<div style=\"float:#{ algn }\">#{ out }</div>"
                end
            else
                out = stln + out
            end

            out
        end
    end

  def check_refs(text)
    db = EntryDB.db
    if db.entry? text.to_sym
      [ @partial.partial_url('MonthEntry',text.to_sym), nil ]
    elsif text =~ /^id:\/\/(.*)/
      [ @partial.partial_url('MonthEntry',$1.to_sym), nil ]
    elsif text =~ /^\[file\](.*)/ || text =~ /^files:\/\/(.*)/
      [ @partial.resolve_url(Partial.config.files_url+$1), nil ]
    elsif text =~ /^tex:\/\/(.*)/
      [ @partial.resolve_url(Partial.config.files_url+$1.hash.to_s.sub(/^-/,'M')), nil ]
    elsif text =~ /^lastfm:\/\/(.*)/
      [ "http://www.last.fm/"+$1, nil ]
    elsif text =~ /^graphviz:\/\/(.*)/
      graphviz_id = $1.to_i
      att = {}
      @graphviz_list[graphviz_id][0].gsub(/([^ ]+)="([^"]+)"/) { att[$1] = $2; }
      cmd = att["cmd"] || "dot";
      rsvg_args = ""
      rsvg_args += " -w #{att["width"]}" if att["width"]
      rsvg_args += " -h #{att["height"]}" if att["height"]
      f = IO.popen("#{cmd} -Tsvg|rsvg #{rsvg_args} - #{Partial.config.archives_dir}/#{@id.to_s}-#{graphviz_id}.png","w")
      f.write(@graphviz_list[graphviz_id][1]);
      f.close
      [ @partial.resolve_url(Partial.config.archives_url+"#{@id.to_s}-#{graphviz_id}.png"), nil ]
    else
      super(text)
    end
  end

  # stolen from rip_offtags
  GRAPHVIZTAG_MATCH = /(?:(<\/(graphviz)>)|(<(graphviz)[^>]*>))(.*?)(?=<\/?(graphviz)|\Z)/mi
  def graphviz_it
    return unless self =~ /<.*>/

    ## strip and encode <pre> content
    codepre, used_offtags = 0, {}
    self.gsub!( GRAPHVIZTAG_MATCH ) do |line|
      if $3
        offtag, aftertag = $4, $5
        codepre += 1
        used_offtags[offtag] = true
        if codepre - used_offtags.length > 0
          @graphviz_list.last[1] << line
          line = ""
        else
          line = "!graphviz://#{ @graphviz_list.length }!"
          @graphviz_list << [$3,aftertag]
        end
      elsif $1 and codepre > 0
        if codepre - used_offtags.length > 0
          @graphviz_list.last[1] << line
          line = ""
        else
          line = $5
        end
        codepre -= 1 unless codepre.zero?
        used_offtags = {} if codepre.zero?
      end
      line
    end
  end
end
