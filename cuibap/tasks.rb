require 'rubygems'
require 'rake'
require 'rake/clean'
require 'erb'
require 'fileutils'

require 'cuibap/config'
require 'cuibap/db'
require 'cuibap/git'

require 'cuibap/partial'

require 'cuibap/month_entry'
require 'cuibap/tag_archive'
require 'cuibap/month_archive'
require 'cuibap/main_page'
require 'cuibap/test_page'
require 'cuibap/year_index'

class GitTask < Rake::FileTask
  private
  def needed?
    return true if super
    @@shamap = git_sha1 unless defined?(@@shamap)
    fn = self.name
    gitname = File.basename(fn)
    return false unless @@shamap.key? gitname
    newsha1 = @@shamap[gitname]
    retval = false
    if File.exists?(fn)
      sha1 = File.readlines(self.name)[0].chomp
      retval = sha1 != newsha1
    else
      retval = true
    end
    retval
  end

  def execute
    super
    @@shamap = git_sha1 unless defined?(@@shamap)
    gitname = File.basename(self.name)
    if @@shamap.key? gitname
      f = File.open(self.name,'w')
      f.puts @@shamap[gitname]
      f.close
    end
  end

  def git_sha1
    ENV['GIT_DIR'] = File.expand_path(C.git_dir)
    shamap = {}
    %x(git ls-tree -r HEAD).chomp.split(/\n/).each do |x|
      shamap[x.split(/\t/)[1]] = x
    end
    shamap
  end

end

def entries_to_tasks (args)
  git_support = !(C.git_dir.nil? || C.git_dir.empty?)
  if git_support == true
    git_dir = File.join(C.cache_dir,'git')
    File.mkdir(git_dir) unless File.directory?(git_dir)
  end
  args.map do |x|
    file "#{C.data_dir}/#{x}.txt"
    if git_support == true
      tn = File.join(git_dir,"#{x}.txt")
      GitTask.define_task(tn)
      task x => tn
    end
    task x => "#{C.data_dir}/#{x}.txt"
  end
end


NB_PrevArchiveMonthLink = ''
NB_NextArchiveMonthLink = ''
NB_PageLinks = ''
NB_MetaTitle = ''
NB_PrevArchiveYearLink = ''
NB_NextArchiveYearLink = ''

# Settings
C.load

# metadata cache
db = EntryDB.new C.data_dir
metadata_file = "#{C.cache_dir}/metadata.yaml"
db.load(metadata_file)
db.build_tags
Partial.load

file C.main_link
file 'rss.xml'
task :main => [ C.main_link, 'rss.xml' ]
task :archive
task :default => [ :main, :archive ]
CLOBBER.include('archives','index.html','test.html',metadata_file)

file metadata_file
desc "Build metadata cache"
task :metadata => metadata_file

task metadata_file => entries_to_tasks(db.all) do |task|
  task.prerequisites.each do |n|
    subtask = Rake::Task[n]
    if subtask.timestamp > task.timestamp
      db[subtask.name.to_sym].load_metadata
    end
  end
  db.save(task.name)
end

directory C.archives_dir

require 'cuibap/tasks/edit.rb'
require 'cuibap/tasks/ls.rb'
require 'cuibap/tasks/new.rb'
require 'cuibap/tasks/rm.rb'
require 'cuibap/tasks/rss.rb'
require 'cuibap/tasks/tags.rb'
require 'cuibap/tasks/test.rb'
require 'cuibap/tasks/git.rb'
require 'cuibap/tasks/grep.rb'
require 'cuibap/tasks/status.rb'
require 'cuibap/tasks/commit.rb'
require 'cuibap/tasks/revert.rb'
