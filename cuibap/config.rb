require 'rubygems'
require 'yaml'

class C
  def C.load(rootdir = '')
    rootdir += '/' unless rootdir.empty?
    @@defaults = YAML::load(File.read(rootdir+'default.yaml'))
    if File.exists? rootdir+'config.yaml'
      @@configs = YAML::load(File.read(rootdir+'config.yaml'))
    else
      @@configs = {}
    end
    @@configs['rootdir'] = rootdir
    append_slash('url')
    append_slash('archives_url')
    append_slash('archives_dir')
    append_slash('tags_url')
    append_slash('files_url')
  end

  def C.append_slash(id)
    @@configs[id] = (get(id) || '') + '/' unless get(id) =~ /\/$/
  end

  def C.get(id)
    return @@configs[id] if @@configs.key? id
    return @@defaults[id] if @@defaults.key? id
    nil
  end

  def C.method_missing(id)
    get(id.to_s)
  end

  def C.get_base_url(level = 0)
    C.absolute_links ? C.url : '../'*level
  end

end
