require 'cuibap/cuibapcloth'

class Entry
  attr_reader :id, :filename

  def initialize(id,db)
    @id = id
    @filename = "#{@id}.txt"
    @db = db
    @title = ''
    @author = ''
    @date = ''
    @desc = ''
    @format = 'raw'
    @tags = []
    @external_tags = []
    @body = ''
    @meta_loaded = false
    @body_loaded = false
  end

  def path
    @db.dir+'/'+@filename
  end

  def load_metadata
    f = File.new(self.path,'r')
    clear_tags
    loop do
      line = f.readline
      @title = $1 if line =~ /^TITLE: *(.*)/
      @author = $1 if line =~ /^AUTHOR: *(.*)/
      @date = $1 if line =~ /^DATE: *(.*)/
      @desc = $1 if line =~ /^DESC: *(.*)/
      @format = $1 if line =~ /^FORMAT: *(.*)/
      add_tags($1) if line =~ /^TAGS: *(.*)/
      break if line =~ /^BODY:/
    end
    @meta_loaded = true
  end

  def load_txt
    is_body = false
    clear_tags
    File.readlines("#{@db.dir}/#{@filename}").each do |line|
      if is_body
        if line =~ /^-----$/
	  is_body = false 
	else
          @body += line
	end
      else
        @title = $1 if line =~ /^TITLE: *(.*)/
        @author = $1 if line =~ /^AUTHOR: *(.*)/
        @date = $1 if line =~ /^DATE: *(.*)/
        @desc = $1 if line =~ /^DESC: *(.*)/
        @format = $1 if line =~ /^FORMAT: *(.*)/
        add_tags($1) if line =~ /^TAGS: *(.*)/
        if line =~ /^BODY:/
          @body = ''
          is_body = true
        end
      end
    end
    @meta_loaded = true
    @body_loaded = true
  end

   def Entry.get_tag_code(tag)
     tag.hash.to_s.sub('-','M')
   end

  def to_html(partial)
    load_txt unless @body_loaded
    if @format == "textile"
      html = CuiBapCloth.new(partial,@body,self.id).to_html
    else
      html = CuiBapCloth.smileys_to_html(partial,@body)
    end
  end

  def to_txt
    txt = <<EOF
TITLE: #{@title}
AUTHOR: #{@author}
DATE: #{@date}
DESC: #{@desc}
FORMAT: #{@format}
TAGS: #{@tags.join(', ')}
-----
BODY:
#{@body}
-----
EOF
  end

  def add_tag(tag)
    tag.strip!
    @tags.push tag unless @tags.member? tag
  end

  def add_tags(tag_string)
    tag_string.split(',').each do |tag|
      add_tag(tag)
    end
  end

  def clear_tags
    @tags = @external_tags
  end

  def add_external_tag(tag)
    tag.strip!
    @external_tags.push(tag) unless @external_tags.member? tag
    @tags.push(tag) unless @tags.member? tag
  end

  def method_missing(symbol)
    return nil unless [ :title, :author, :desc, :date, :format, :tags ].member? symbol

    return @db.cache[@id][symbol] if @db.cache.key? @id and not @meta_loaded

    return instance_variable_get("@#{symbol}")
  end

  def dirty?
    @meta_loaded or @body_loaded
  end

  # Human-readable ID
  def hid
    @db.get_hid(@id)
  end
end

class EntryDB
  attr_reader :tags, :cache, :dir

  @@db = nil
  def EntryDB.db
    @@db
  end

  def initialize(dir)
    @entries = {}
    @tags = {}
    @cache = {}
    @dir = dir
    @@db = self

    Dir.mkdir(dir) unless File.directory? dir
    Dir.new(dir).entries.each do |e|
      if e =~ /(.*)\.txt$/
        id = $1.to_sym
        entry = Entry.new(id,self)
#        entry.load_txt
        @entries[entry.id] = entry
      end
    end

    Dir.new(dir).entries.each do |e|
      if e =~ /cat_.*.db/
        f = File.readlines(dir+'/'+e)
	tag = f[0].chomp
	f[1..f.length-1].each do |filename|
          id = id_from_path(filename)
	  @entries[id].add_external_tag(tag) if @entries.key? id
	end
      end
    end
    @sorted_entries = @entries.keys.map { |x| x.to_s }.sort { |x,y| y <=> x }
  end

  def id_from_path(filename)
    $1.to_sym if File.basename(filename) =~ /^(.*)\.txt$/
  end

  def get_hid(id)
    # One-based index
    @sorted_entries.index(id.to_s) + 1
  end

  def id_from_hid(hid)
    return nil unless !hid.nil? && hid.is_a?(Integer) && hid >= 0 && hid < @sorted_entries.length
    @sorted_entries[hid-1].to_sym
  end

  def [](id)
    @entries[id]
  end

  def entry?(id)
    @entries.key? id
  end

  def entries_num
    @entries.length
  end

  def empty?
    @entries.empty?
  end

  def all
    @entries.keys
  end

  def load(filename)
    if File.exists?(filename)
      @cache = YAML::load(File.read(filename))
    end
  end

  def save(filename)
    # traverse and update the metadata cache
    for e in @entries.values
      if e.dirty?
        @cache[e.id] = {}
        @cache[e.id][:title] = e.title
        @cache[e.id][:author] = e.author
        @cache[e.id][:desc] = e.desc
        @cache[e.id][:date] = e.date
        @cache[e.id][:format] = e.format
        @cache[e.id][:tags] = e.tags
      end
    end
    f = File.new(filename,'w')
    f.write(YAML::dump(@cache))
    f.close
  end

  def remove_tag(id,tag)
    tag = tag.strip
    return unless @tags.key? tag
    @tags[tag].delete(id)
  end

  def build_tags
    @tags = build_custom_tags(@entries.keys)
  end

  def build_custom_tags(idlist)
    tags = {}
    for e in idlist
      for tag in @entries[e].tags
        tag = tag.strip
        tags[tag] = [] unless tags.key? tag
        tags[tag].push e
      end
    end
    tags
  end

  def top_entries(max_entries)
    @sorted_entries[0..max_entries-1].map { |x| x.to_sym }
  end

  def nth(n)
    @sorted_entries[n].to_sym
  end

  def entries_by_prefix(prefix)
    @sorted_entries.grep(Regexp.new('^'+prefix)).map {|x| x.to_sym }
  end

  def entries_by_tag(tag)
    if @tags[tag].nil?
      []
    else
      @tags[tag].sort { |x,y| y.to_s <=> x.to_s }
    end
  end

  def get_lastest_entry
    @sorted_entries[0].to_sym
  end

  def get_oldest_entry
    @sorted_entries[-1].to_sym
  end

  def get_min_year
    @sorted_entries[-1][0,4].to_i
  end

  def get_max_year
    @sorted_entries[0][0,4].to_i
  end
  
  def get_max_month(year)
    @sorted_entries.grep(Regexp.new(sprintf('^%04d-',year)))[0][5,2].to_i
  end
  
  def get_min_month(year)
    @sorted_entries.grep(Regexp.new(sprintf('^%04d-',year)))[-1][5,2].to_i
  end
end
